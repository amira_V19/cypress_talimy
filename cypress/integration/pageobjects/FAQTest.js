/// <reference types="Cypress" />
import FAQPage from "./FAQPage"



//Starting test suite
describe('My second Test ', function () {

    before(function () {
        cy.fixture('Inputdata').then(function (testdata) {
            this.testdata = testdata
        })
    })




    ////Starting test case
    it('Validate common Questions and Answers', function () {


        const FAQ = new FAQPage();
        FAQ.navigate();
        FAQ.getFoundation();
        FAQ.getFoundationAnswer(this.testdata.Answer1);
        FAQ.getPortal();
        FAQ.getPortalAnswer(this.testdata.Answer2);
        FAQ.getUsers();
        FAQ.getUsersAnswer(this.testdata.Answer3);
        FAQ.getMaterial();
       // FAQ.getMaterialAnswer(this.testdata.Answer4);
        FAQ.getFreeLearning();
        //FAQ.getFreeLearningAnswer(this.testdata.Answer5);
        FAQ.getTa3limyMethods();
        //FAQ.getTa3limyMethodsAnswer(this.testdata.Answer6);
        FAQ.getPointsLeaderBoard();
        //FAQ.getPointsLeaderBoardAnswer(this.testdata.Answer7);


    })

})