
class FAQPage {

    navigate() {

        //this command will visit the given URL 

        cy.visit('https://ta3limy.com/faq')
    }




    getFoundation() {

        cy.get('p').contains('ما هي المؤسسة ؟').click();
    }
    getFoundationAnswer(Answer1) {

        cy.get('p').contains('مؤسسة ڤودافون مصر لتنمية المجتمع هي أول مؤسسة لا تهدف للربح في مجال الاتصالات في مصر وهي تابعة لوزارة التضامن الاجتماعي وتختلف عن قسم المسؤولية المجتمعية الخاص بالشركة.')
            .invoke('text')
            .then((text1) => {

                expect(text1).to.eq(Answer1)
            })

    }

    getPortal() {

        cy.get('p').contains('ما هو تعليمي ؟').click();
    }
    getPortalAnswer(Answer2) {

        cy.get('p').contains('تعليمي هي منصة إلكترونية تعليمية مجانية للطلاب وأولياء الأمور والمعلمين')
            .invoke('text')
            .then((text1) => {

                expect(text1).to.eq(Answer2)
            })

    }


    getUsers() {

        cy.get('p').contains('من المستفيد من تعليمي ؟').click();
    }

    getUsersAnswer(Answer3) {

        cy.get('p').contains('أولياء الأمور والطلاب في المرحلة الأولي وستستهدف المرحلة القادمة تقديم محتوى للمعلمين')
            .invoke('text')
            .then((text1) => {

                expect(text1).to.eq(Answer3)
            })

    }
    getMaterial() {

        cy.get('p').contains('ما هو المحتوي الموجود في تعليمي؟').click();
    }

    getMaterialAnswer(Answer4) {

        cy.get('p').contains('لأولياء الأمور يوجد:')
            .invoke('text')
            .then((text1) => {

                expect(text1).to.eq(Answer4)
            })

    }
    getFreeLearning() {

        cy.get('p').contains('هل تعليمي مجاني ؟').click();
    }
    getFreeLearningAnswer(Answer5) {

        cy.get('p').contains('تعليمي مجاني لجميع الأفراد بشكل عام ويتميز عملاء ڤودافون بعدم السحب من باقة الانترنت بشكل خاص')
            .invoke('text')
            .then((text1) => {

                expect(text1).to.eq(Answer5)
            })

    }
    getTa3limyMethods() {

        cy.get('p').contains('كيف أقوم باستخدام تعليمي ؟').click();
    }

    getTa3limyMethodsAnswer(Answer6) {

        cy.get('p').contains('تظهر صفحة التسجيل تحتاج لملء التالي')
            .invoke('text')
            .then((text1) => {

                expect(text1).to.eq(Answer6)
            })

    }

    getPointsLeaderBoard() {

        cy.get('p').contains('كيف احصل على النقاط في لوحة المتصدرين ؟').click();
    }
    getPointsLeaderBoardAnswer(Answer7) {

        cy.get('p').contains('يتم تحديد الأشخاص الموجودين بلوحة المتصدرين على منصة تعليمي عن طريق الطرق التالية:')
            .invoke('text')
            .then((text1) => {

                expect(text1).to.eq(Answer7)
            })

    }
}
export default FAQPage
