/// <reference types="Cypress" />
import RegisterPage from "./RegisterPage"



//Starting test suite
describe('My First Test ', function () {


    before(function () {
        cy.fixture('Inputdata').then(function (testdata) {
            this.testdata = testdata
        })
    })

    ////Starting test case
    it('Register as a parent', function () {


        const Register = new RegisterPage();
        Register.navigate();
        Register.submit();
        Register.selectUser();
        Register.selectAgeRange();
        Register.selectResidence();
        Register.clickOnRegister();
        Register.clickOnRegisterByMobile();
        Register.enterFirstName(this.testdata.firstname);
        Register.enterLastName(this.testdata.lastname);
        Register.enterMobileNumber(this.testdata.mobilenumber);
        Register.selectGender();
        Register.enterPassword(this.testdata.password);

        Register.enterpasswordConfirmation(this.testdata.password);
        Register.clickOneTrmsAndConditionsCheck();
        //Register.clickOnRecaptcha();
        Register.clickOnRegisterFinal();
        // cy.url().should('be.equal', this.testdata.adminUrl)

    })

})