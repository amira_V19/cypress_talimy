
class RegisterPage {

    navigate() {

        //this command will visit the given URL 
        cy.visit('https://www.ta3limy.com')
    }




    submit() {


        cy.get('*[class^="e1a5eqzl0 css-sdxfg erkcdwb4"]').click()
    }



    selectUser() {
        cy.get('[id=parent]').click()

    }

    selectAgeRange() {


        cy.get('[type="radio"]').first().check() // Check first radio element

    }


    selectResidence() {

        cy.get('[id=true]').click()
    }


    clickOnRegister() {
        cy.get('[type=submit]').click()
    }



    clickOnRegisterByMobile() {
        cy.get('[alt="عن طريق رقم المحمول"]').click()
    }



    enterFirstName(firstname) {
        cy.get('[id=firstName]').clear()
        cy.get('[id=firstName]').type(firstname);
        return this
    }



    enterLastName(lastname) {
        cy.get('[id=lastName]').clear()
        cy.get('[id=lastName]').type(lastname);
        return this
    }

    enterMobileNumber(mobilenumber) {
        cy.get('[id=mobileNumber]').clear()
        cy.get('[id=mobileNumber]').type(mobilenumber);
        return this
    }

    selectGender() {


        cy.get('[type="radio"]').first().check() // Check first radio element

    }

    enterPassword(password) {
        cy.get('[id=password]').clear()
        cy.get('[id=password]').type(password);
        return this
    }


    enterpasswordConfirmation(password) {
        cy.get('[id=passwordConfirmation]').clear()
        cy.get('[id=passwordConfirmation]').type(password);
        return this
    }

    clickOneTrmsAndConditionsCheck() {
        cy.wait(5000);
        cy.get('[disabled]').click({ force: true });
        cy.get('#termsAndConditionsCheck-input').click();
        //cy.get('[type="checkbox"]').check() // Check checkbox element  
        // cy.get('[id=termsAndConditionsCheck]').click();

        cy.get('#termsAndConditionsCheck-input').click();



    }

    clickOnRecaptcha() {
        // cy.get('[id=recaptcha-anchor]').check();
        cy.wait(500);
        cy.get('*[class^="recaptcha-checkbox-checkmark').click();




    }

    clickOnRegisterFinal() {
        cy.get('[data-cy=submitButton]').click()
    }


}

export default RegisterPage
